﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerPools : MonoBehaviour {

	public GameObject poolObject;
	public int pooledamount;
	List<GameObject> poolobjects;
	public float xRotation, yRotation, zRotation;


	void Start()
	{
		poolobjects= new List<GameObject>();
		for(int i=0; i<pooledamount; i++)
		{
			GameObject obj = (GameObject)Instantiate(poolObject);
			obj.SetActive(false);
			poolobjects.Add(obj);
		}
	}
	
	public GameObject GetPooledObject()
	{
		for(int i=0; i < poolobjects.Count; i++)
		{
			if(!poolobjects[i].activeInHierarchy)
			{
				return poolobjects[i];
			}
		}
			GameObject obj = (GameObject)Instantiate(poolObject);
			obj.SetActive(false);
			poolobjects.Add(obj);
			return obj;

	}
	
		public void RotationOfTower()
	{
		zRotation= Random.Range(15, 45);
		transform.Rotate(xRotation, yRotation, zRotation);
		
	}
}
