﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grab : MonoBehaviour {

	private Vector3 mOffset; 

	void OnMouseDown()
	{
		mOffset = gameObject.transform.position - GetMousePos();
	}
	private Vector3 GetMousePos()
	{
		Vector3 mousePoint= Input.mousePosition;

		return Camera.main.ScreenToWorldPoint(mousePoint);
	}
	void OnMouseDrag()
	{
		transform.position = GetMousePos() + mOffset;
	}
}
